const axios = require('axios');
const express = require('express');
const { performance } = require('perf_hooks');

const app = express();
const dummyObj = require('./dummy');
const dummyObj2 = require('./dummy2');
const ejs = require('ejs');

app.get('/', (req, res, next) => {
    console.log('in home route');

    let time = performance.now();
    axios({
        method: 'GET',
        url: 'https://awsrekognitionmcahemal.s3.ap-south-1.amazonaws.com/temp.ejs',
      })
        .then(function (response) {
            time2 = performance.now();
            console.log(`${(time2 - time)} milliseconds --- Getting from S3`);
            var course = {_id: 1, target_url: 'hi'};
            var tasks = [{name: 'Server Side Rendering', duration: '1 day'}, {name: 'Client Side Rendering', duration: '2 days'}, {name: 'Static Site Generation', duration: '3 days'}];
            var organization = {name: "Edmingle", description: "LMS Provider"};
            const template_dependencies = {
                heading: "The Test Page!",
                course: course,
                organization: organization,
                tasks: tasks,
                data: dummyObj.dummyData,
                body: dummyObj2.dummyBodyData
            }
            let template = ejs.render(response.data, template_dependencies);
            return template;
            
        })
        .then (page => {
            console.log(`${(performance.now() - time2)} milliseconds --- Rendering`);
            res.send(page);
        })
        .catch(err => console.log(err));

});

//listen on port 80 when no production port environment variable exisits.
app.listen(80, (error) => {
    if (error) {
        console.log(error);
    }
    console.log('Server started on PORT 80!');
});

